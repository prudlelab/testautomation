package TestAutomation.testng.exception;

public class MemberIsInactive extends Exception {

	public MemberIsInactive() {
		super();
	}
	
	public MemberIsInactive(String message) {
		super(message);
	}
}
