package testAutomation.restAss;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.testng.Assert.assertEquals;

import java.util.Iterator;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.*;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class TestPost {

	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = "https://jsonplaceholder.typicode.com";
		
	}
	
	
	@Test
	public static void testPostObjectGETRespose(){
		
		given()  // Current situation
        .when()  // Execute
        	.get("/posts")
        .then()  // verification
        	.statusCode(200);
		    	
	}
	
	@Test
	public static void testPostObjectGETResponseWithID()
	{
		
		given() 
			.pathParam("id", 3)
        .when()  // Execute
        	.get("/posts/{id}")
        .then()  // verification
        	.statusCode(200)
        	.body("userId", equalTo(1))
        	.body("title", equalTo("ea molestias quasi exercitationem repellat qui ipsa sit aut"));
		    
	}
	
	@Test
	public static void testPostObjectPOSTResponse()
	{
		String postRequestBody = "{\r\n" + 
				"        \"userId\": 7,\r\n" + 
				"        \"title\": \"Sample Request for REST ASSURED\",\r\n" + 
				"        \"body\": \"Sample Request for REST ASSURED USING POSTMAN\"\r\n" + 
				"    }";
		
		given() 
			.header("Content-type","application/json")
			.and()
			.body(postRequestBody)
        .when()  // Execute
        	.post("/posts")
        .then()  // verification
        	.statusCode(201)
        	.body("id", equalTo(101));
        	
		    
	}
	
	
	
	
	
	
	
	
	@Test(dataProvider = "userIDDataProvider")
	public static void testPostObjectGETResponseWithParamSearch(int userID)
	{	
		List<Object> posts = given() 
			.queryParam("userId", userID)
        .when()  // Execute
        	.get("/posts")
        .then()  // verification
        	.statusCode(200)
        	.body("[0].userId", equalTo(userID))
        	.extract().jsonPath().getList("$");
		
		
		assertEquals(posts.size(), 10);
			
		    
	}
	
	
	
	
	@DataProvider(name = "userIDDataProvider")
	public Object[][] getUserIDDataProvider()
	{
		return new Object[][]{{2},{3},{4},{5}};
	}
	
	
}
