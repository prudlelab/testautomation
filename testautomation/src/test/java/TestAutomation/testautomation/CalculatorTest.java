package TestAutomation.testautomation;

import static org.junit.Assert.assertThrows;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class CalculatorTest extends TestCase {

	Calculator c ;
		
	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		
		
		c = new Calculator();
		
	}
	
	@Test
	public void testAddWithNormalValue() {
		
		int result = c.add(5, 9);
		
		assertEquals(14, result);
		
	}
	
	@Test
	public void testAddWithNegativeValue() {
		int result = c.add(-5, 8);
		
		assertEquals(3, result);
		
	}
	
	@Test
	public void testAddWithBothNegativeValue() {
		Integer result = c.add(-5, -8);
		
		assertNotNull(result);
		
		assertEquals(new Integer(-13), result);
		
		
	}
		
	@Test
	public void testDivWithSimpleValue() {
		
		int result = c.div(10,2 );
		assertEquals(5, result);
		
	}
	
	@Test
	public void testDivWithZeroValue() {

		Integer result = c.div(10,0 );
		assertNull(result);
		
	}

	@Test
	public void testDivWithZeroValueException() {
		
		assertThrows(ArithmeticException.class, () -> c.divWOZeroHandling(10,0));
		
	}
	
	
	
}
