package TestAutomation.testautomation;


import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest  extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    public void testApp()
    {
        assertTrue( true );
        
    }
    
   @org.junit.Test
    public void testStringEquality()
    {
    	
    	App app = new App();
    	String greeting = app.sendGreetingMessage();
    	
    	assertEquals("Hello Team", greeting);
    }
}
